

import Foundation
import UIKit

let BaseURL = "https://39219992.ngrok.io/eshiksa/apiLogin/login"

//let BaseURL = "http://app.colonyworld.com/colony/index.php?plugin=MobileAPI&action=getRequest"

let SocietyID = "35"

let PrebillPdfUrl = "http://app.colonyworld.com/colony/reports/payment/prebill/"

let PayPdfUrl = "http://app.colonyworld.com/colony/reports/payment/invoices/"

let GalleryUrl = "http://app.colonyworld.com/colony/upload/images/gallery/"

let AppVersion = "http://app.colonyworld.com/colony/index.php?plugin=MobileAPI&action=get_app_verison"

let CircularUrl = "http://app.colonyworld.com/colony/upload/circular/"




class APICalls: NSObject {
    class func requestForCurranciesWith(jsonRequestParams : [String : Any], onSuccess success: @escaping (_ response : [String : Any]) -> (Void), onFailure failure: @escaping (_ error : Error?) -> (Void)) {
        let url = BaseURL
        WebCommHandler.requestFromWebServiceWith(jsonRequestParams: jsonRequestParams, requestURL: url, requestMethod: "POST", authorization: true, onSuccess: { (responseDict) -> (Void) in
            success(responseDict)
        }) { (error) -> (Void) in
            failure(error)
        }
    }
}
