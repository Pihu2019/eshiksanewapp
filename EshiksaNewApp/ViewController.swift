//
//  ViewController.swift
//  EshiksaNewApp
//
//  Created by shayam on 27/06/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ViewController: UIViewController {

    @IBOutlet var tblJSON: UITableView!
    var arrRes = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Almofire implementation
//        Alamofire.request("http://api.androidhive.info/contacts/").responseData { (resData) -> Void in
//            print(resData.result.value!)
//            let strOutput = String(data: resData.result.value! , encoding:String.Encoding.utf8)
//            print(strOutput)
        
           //SwiftyJSON implementation
            Alamofire.request("http://api.androidhive.info/contacts/").responseData(completionHandler: { (responseData) -> Void in
                if((responseData.result.value) != nil){
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    print(swiftyJsonVar)
                    
                    if let resData = swiftyJsonVar["contacts"].arrayObject {
                        self.arrRes = resData as! [[String:AnyObject]]
                    }
                    if self.arrRes.count > 0 {
                        self.tblJSON.reloadData()
                    }
                }
            })
        }
    //delegate methods of table
    
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "jsonCell")!
        var dict = arrRes [indexPath.row]
        cell.textLabel?.text = dict["name"] as? String
        cell.detailTextLabel?.text = dict["email"] as? String
        return cell
    }
    
    func  tableView(tableView: UITableView, numberfRowsInSection section: Int) -> Int {
        return arrRes.count
    }
}



















/*
 
1)Rishabh
2)Anil
3)Nitin
4)Avinash
5)Ravi
6)Yash
7)Rohit
8)Navneet
9)Manoranjan
10)Ram
11)Pramod
12)Avdhesh/Harshal
 
 
13)Shweta
14)Ankita
15)Priya
16)Sneha

*/
